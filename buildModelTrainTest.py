import os
from time import time

import joblib
import matplotlib.pyplot as plt
import numpy
import numpy as np
import scipy.signal as signal
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Dropout, MaxPooling2D, Flatten, ConvLSTM2D, LSTM, GRU
from keras.layers.convolutional import Conv1D, MaxPooling1D
from keras.models import Sequential
from sklearn.metrics import accuracy_score, precision_score, precision_recall_curve, precision_recall_fscore_support, \
    recall_score, f1_score, classification_report, roc_curve, auc
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.svm import SVC, LinearSVC
from hmmlearn import hmm
from buildDATASET import load_Dataset_2_from_h5file, load_Dataset_3_from_h5file, log_comment_toFile

PLOT = False
VERBOSE = 1
time_step = 29

# 0.04s *29 *3
def get_feat_need_col(feat, trainX, testX):
    # 'mfcc13', 'lpcc13', 'plp13'
    if feat == 'lpcc':
        trainX = trainX[:, [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]]
        testX = testX[:, [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]]
    elif feat == 'mfcc':
        trainX = trainX[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]
        testX = testX[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]
    elif feat == 'plp':
        trainX = trainX[:, [26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38]]
        testX = testX[:, [26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38]]
    elif feat == 'lpcc_mfcc':
        trainX = trainX[:,
                 [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]
        testX = testX[:, [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]
    elif feat == 'lpcc_plp':
        trainX = trainX[:,
                 [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
                  38]]
        testX = testX[:,
                [13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
                 38]]
    elif feat == 'mfcc_plp':
        trainX = trainX[:,
                 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38]]
        testX = testX[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38]]
    else:
        pass
    return trainX, testX


def get_step_data(dataX, dataY, step, array_dim_num=3):
    feat_dim = numpy.shape(dataX)[-1]
    finalX = []
    finalY = []
    for item_y in range(0, len(dataY), step):
        if list(dataY[item_y:item_y + step]).count(1) == step:
            finalX.append(dataX[item_y:item_y + step])
            finalY.append(1)
        if list(dataY[item_y:item_y + step]).count(0) == step:
            finalX.append(dataX[item_y:item_y + step])
            finalY.append(0)
    finalX = numpy.array(finalX)
    finalY = numpy.array(finalY)
    if array_dim_num == 4:
        finalX = numpy.reshape(finalX, (-1, 1, 1, step, feat_dim))
    return finalX, finalY


def get_evaluation_report(testY, predictY):
    print('predictY:singNUM %i nosingNUM %i' % ((predictY == 1).sum(), (predictY == 0).sum()))
    acc = accuracy_score(testY, predictY)
    pre = precision_score(testY, predictY, pos_label=1)
    rec = recall_score(testY, predictY, pos_label=1)
    f1 = f1_score(testY, predictY, pos_label=1)
    # if PLOT:
    #     fpr, tpr, thresholds = roc_curve(testY, predictY_proba[:, 1], pos_label=1)
    #     roc_auc = auc(fpr, tpr)
    #     plt.plot(fpr, tpr, lw=1, label='ROC (area = %0.2f)' % roc_auc)
    #     plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
    #     plt.xlim([-0.05, 1.05])
    #     plt.ylim([-0.05, 1.05])
    #     plt.xlabel('False Positive Rate')
    #     plt.ylabel('True Positive Rate')
    #     plt.title('Receiver operating characteristic example')
    #     plt.legend(loc="lower right")
    #     plt.show()
    if VERBOSE:
        print('=======classification_report========')
        print(classification_report(testY, predictY))
    # precision, recall, thresholds = precision_recall_curve(testY, predictY_proba[:, 1], pos_label=1)
    #     print('=======precision, recall, thresholds========')
    #     print(precision, recall, thresholds)
    #     pre_rec_f1 = precision_recall_fscore_support(testY, predictY, pos_label=1, labels=[1, 2])
    #     print('=======pre_rec_f1========')
    #     print(pre_rec_f1)
    #     print('=======acc, pre, rec, f1========')
    #     print(acc, pre, rec, f1)
    return acc, pre, rec, f1


def filter_inf_nan(trainX, trainY, testX, testY):
    trainX = np.nan_to_num(trainX)
    testX = np.nan_to_num(testX)
    return trainX, trainY, testX, testY


def mid_filter(list_sig, num=3):
    return signal.medfilt(list_sig, num)


def hmm_smoothing(predictY_prob):
    np.random.seed(1007)
    model = hmm.GMMHMM(n_components=2, n_iter=40, n_mix=45)
    model.fit(predictY_prob, )
    predictY = model.predict(predictY_prob)  # 0.871955719557 #0.841328413284
    return predictY


def buildSVM(trainX, trainY, testX, testY):
    clf = LinearSVC()
    scaling = MinMaxScaler(feature_range=(-1, 1)).fit(trainX)
    trainX = scaling.transform(trainX)
    testX = scaling.transform(testX)
    print('Y:singNUM %i nosingNUM %i' % ((trainY == 1).sum(), (trainY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((testY == 1).sum(), (testY == 0).sum()))
    print('fitting svm...')
    print('sing samples num:%i, nosing samples num:%i' % (np.sum(trainY), (len(trainY) - np.sum(trainY))))
    clf.fit(trainX, trainY)
    print('predict...')
    predictY = clf.predict(testX)
    print('gen report of evaluation...')
    acc, pre, rec, f1 = get_evaluation_report(testY, predictY)
    return clf, acc, pre, rec, f1


def buildDNN(trainX, trainY, testX, testY, validX, validY):
    print('Y:singNUM %i nosingNUM %i' % ((trainY == 1).sum(), (trainY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((testY == 1).sum(), (testY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((validY == 1).sum(), (validY == 0).sum()))
    feat_dim = numpy.shape(trainX)[-1]
    print(feat_dim)
    model = Sequential()
    model.add(Dense(13, input_dim=feat_dim, activation='relu'))
    model.add(Dense(1, activation='sigmoid', init='uniform'))
    model.compile(loss='binary_crossentropy',
                  optimizer='sgd',
                  metrics=['binary_accuracy', 'precision', 'recall'])
    callbacks = [EarlyStopping(monitor='val_loss', patience=2, verbose=0)]
    model.fit(trainX, trainY, batch_size=128, nb_epoch=1000, callbacks=callbacks, validation_data=(validX, validY),
              verbose=VERBOSE, shuffle=False)
    print(model.summary())
    loss_and_metrics = model.evaluate(validX, validY, batch_size=128, verbose=0)
    print ('loss metrics:', loss_and_metrics)
    predictY = model.predict_classes(testX)
    print('gen report of evaluation...')
    acc, pre, rec, f1 = get_evaluation_report(testY, predictY)
    return model, acc, pre, rec, f1


def buildLSTM(trainX, trainY, testX, testY, validX, validY):
    print('Y:singNUM %i nosingNUM %i' % ((trainY == 1).sum(), (trainY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((testY == 1).sum(), (testY == 0).sum()))
    trainX, trainY = get_step_data(trainX, trainY, time_step, array_dim_num=3)
    testX, testY = get_step_data(testX, testY, time_step, array_dim_num=3)
    validX, validY = get_step_data(validX, validY, time_step, array_dim_num=3)
    feat_dim = numpy.shape(trainX)[-1]
    model = Sequential()
    nb_filter_1, nb_row_1, nb_col_1, drop_rate = feat_dim, 1, 4, 0.2  # 256 for three feat_dim for other
    model.add(LSTM(feat_dim, input_shape=(time_step, feat_dim)))
    model.add(Dropout(drop_rate))
    model.add(Dense(1, activation='sigmoid', init='uniform'))
    model.compile(loss='binary_crossentropy',
                  optimizer='sgd',
                  metrics=['accuracy'])
    callbacks = [EarlyStopping(monitor='val_loss', patience=10, verbose=0)]
    model.fit(trainX, trainY, batch_size=128, nb_epoch=1000, callbacks=callbacks, validation_data=(validX, validY),
              verbose=VERBOSE)
    print(model.summary())
    loss_and_metrics = model.evaluate(validX, validY, batch_size=128, verbose=0)
    print ('loss metrics:', loss_and_metrics)
    predictY = model.predict_classes(testX)
    print('gen report of evaluation...')
    acc, pre, rec, f1 = get_evaluation_report(testY, predictY)
    return model, acc, pre, rec, f1


def buildGRU(trainX, trainY, testX, testY, validX, validY):
    trainX, trainY = get_step_data(trainX, trainY, time_step, array_dim_num=3)
    testX, testY = get_step_data(testX, testY, time_step, array_dim_num=3)
    validX, validY = get_step_data(validX, validY, time_step, array_dim_num=3)
    print('Y:singNUM %i nosingNUM %i' % ((trainY == 1).sum(), (trainY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((testY == 1).sum(), (testY == 0).sum()))
    feat_dim = numpy.shape(trainX)[-1]
    model = Sequential()
    nb_filter_1, nb_row_1, nb_col_1, drop_rate = feat_dim, 1, 4, 0.2  # 256 for three feat_dim for other
    model.add(GRU(feat_dim, input_shape=(time_step, feat_dim)))
    model.add(Dropout(drop_rate))
    model.add(Dense(1, activation='sigmoid', init='uniform'))
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['binary_accuracy', 'precision', 'recall'])
    callbacks = [EarlyStopping(monitor='val_loss', patience=2, verbose=0)]
    model.fit(trainX, trainY, batch_size=128, nb_epoch=1000, callbacks=callbacks, validation_data=(validX, validY),
              verbose=VERBOSE)
    print(model.summary())
    loss_and_metrics = model.evaluate(validX, validY, batch_size=128, verbose=0)
    print ('loss metrics:', loss_and_metrics)
    predictY = model.predict_classes(testX)
    print('gen report of evaluation...')
    acc, pre, rec, f1 = get_evaluation_report(testY, predictY)
    return model, acc, pre, rec, f1


def buildCNN(trainX, trainY, testX, testY, validX, validY):
    trainX, trainY = get_step_data(trainX, trainY, time_step, array_dim_num=3)
    testX, testY = get_step_data(testX, testY, time_step, array_dim_num=3)
    validX, validY = get_step_data(validX, validY, time_step, array_dim_num=3)
    print('Y:singNUM %i nosingNUM %i' % ((trainY == 1).sum(), (trainY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((testY == 1).sum(), (testY == 0).sum()))
    feat_dim = numpy.shape(trainX)[-1]
    model = Sequential()
    model.add(Conv1D(4, 4, input_shape=(time_step, feat_dim), activation='relu'))
    model.add(MaxPooling1D(2))
    model.add(Conv1D(4, 4, activation='relu'))
    model.add(MaxPooling1D(2))
    model.add(Flatten())
    model.add(Dense(1, activation='sigmoid', init='uniform'))
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['binary_accuracy', 'precision', 'recall'])
    print(model.summary())
    callbacks = [EarlyStopping(monitor='val_loss', patience=2, verbose=0)]
    model.fit(trainX, trainY, batch_size=128, nb_epoch=100000, callbacks=callbacks, validation_data=(validX, validY),
              verbose=VERBOSE)
    print(model.summary())
    loss_and_metrics = model.evaluate(validX, validY, batch_size=128, verbose=0)
    print ('loss metrics:', loss_and_metrics)
    predictY = model.predict_classes(testX)
    print('gen report of evaluation...')
    acc, pre, rec, f1 = get_evaluation_report(testY, predictY)
    return model, acc, pre, rec, f1


def buildLRCN(trainX, trainY, testX, testY, validX, validY, post_mesthods='median'):
    trainX, trainY = get_step_data(trainX, trainY, time_step, array_dim_num=4)
    testX, testY = get_step_data(testX, testY, time_step, array_dim_num=4)
    validX, validY = get_step_data(validX, validY, time_step, array_dim_num=4)
    print('Y:singNUM %i nosingNUM %i' % ((trainY == 1).sum(), (trainY == 0).sum()))
    print('Y:singNUM %i nosingNUM %i' % ((testY == 1).sum(), (testY == 0).sum()))
    feat_dim = numpy.shape(trainX)[-1]
    model = Sequential()
    nb_filter_1, nb_row_1, nb_col_1, drop_rate = 256, 1, 4, 0.2  # 256 for three feat_dim for other
    model.add(
        ConvLSTM2D(nb_filter_1, nb_row_1, nb_col_1, input_shape=(1, 1, time_step, feat_dim), activation='relu'))
    model.add(Dropout(drop_rate))
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Dropout(drop_rate))
    model.add(Flatten())
    model.add(Dense(200, activation='relu'))
    model.add(Dropout(drop_rate))
    model.add(Dense(50, activation='relu'))
    model.add(Dropout(drop_rate))
    model.add(Dense(1, activation='sigmoid', init='uniform'))
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['binary_accuracy'])

    callbacks = [EarlyStopping(monitor='val_loss', patience=2, verbose=0)]
    model.fit(trainX, trainY, batch_size=128, nb_epoch=1000, callbacks=callbacks, validation_data=(validX, validY),
              verbose=VERBOSE)
    print(model.summary())
    loss_and_metrics = model.evaluate(validX, validY, batch_size=128, verbose=0)
    print ('loss metrics:', loss_and_metrics)
    predictY = model.predict_classes(testX)
    predictY_prob = model.predict_proba(testX)
    if post_mesthods == 'median':
        newPreY = []
        for item in predictY:
            newPreY.extend(list(item))
        for dur in range(3, 8, 2):
            predictY = mid_filter(newPreY, dur)
    elif post_mesthods == 'hmm':
        predictY = abs(1 - hmm_smoothing(predictY_prob))
    print('gen report of evaluation...')
    log_comment_toFile('gen report of evaluation...')
    acc, pre, rec, f1 = get_evaluation_report(testY, predictY)
    return model, acc, pre, rec, f1


def load_dataset(dataset_h5_path):
    if 'jamendo' in dataset_h5_path:
        trainX, trainY, testX, testY, validX, validY = load_Dataset_3_from_h5file(dataset_h5_path)
        # get the train test valid
    else:
        trainX, trainY, testX, testY = load_Dataset_2_from_h5file(dataset_h5_path)
        validX, validY = testX, testY  # TODO
        # get the cross datasets train test
    return trainX, trainY, testX, testY, validX, validY


def save_model_result_to_report(dataset_h5_path, feat, model_name, acc, pre, rec, f1):
    model_saved_path = dataset_h5_path.replace('.h5', '_' + feat + '_' + model_name + '.model')
    comment = '\n' + dataset_h5_path + '\t' + model_saved_path + '\nusing feature:' + feat + '\n acc:%.3f, pre:%.3f, rec:%.3f, f1:%.3f\n' % (
        acc, pre, rec, f1)
    log_comment_toFile(comment, 'report.txt')
    return 0


def use_diff_features_build_SVM_Model_compare_it(dataset_h5_path):
    model_name = 'SVM'
    for feat in ['lpcc', 'mfcc', 'plp', 'lpcc_mfcc', 'lpcc_plp', 'mfcc_plp', 'lpcc_mfcc_plp']:
        trainX, trainY, testX, testY, validX, validY = load_dataset(dataset_h5_path)
        print (trainX.shape, testX.shape)
        trainX, testX = get_feat_need_col(feat, trainX, testX)
        trainX, trainY, testX, testY = filter_inf_nan(trainX, trainY, testX, testY)
        clf, acc, pre, rec, f1 = buildSVM(trainX, trainY, testX, testY)
        model_saved_path = dataset_h5_path.replace('.h5', '_' + feat + '_svm.model')
        joblib.dump(clf, model_saved_path)
        save_model_result_to_report(dataset_h5_path, feat, model_name, acc, pre, rec, f1)
    return 0


def use_diff_models_same_feat_compare_it(feat, dataset_h5_path):
    trainX, trainY, testX, testY, validX, validY = load_dataset(dataset_h5_path)
    trainX, trainY, testX, testY = filter_inf_nan(trainX, trainY, testX, testY)
    validX, validY, validX, validY = filter_inf_nan(validX, validY, validX, validY)
    trainX, testX = get_feat_need_col(feat, trainX, testX)
    validX, validX = get_feat_need_col(feat, validX, validX)
    print (testX[:10], testY[:10])
    print('# SVM')
    clf, acc, pre, rec, f1 = buildSVM(trainX, trainY, testX, testY)
    save_model_result_to_report(dataset_h5_path, feat, 'SVM', acc, pre, rec, f1)

    print('# DNN')
    clf, acc, pre, rec, f1 = buildDNN(trainX, trainY, testX, testY, validX, validY)
    save_model_result_to_report(dataset_h5_path, feat, 'DNN', acc, pre, rec, f1)

    print('# CNN')
    clf, acc, pre, rec, f1 = buildCNN(trainX, trainY, testX, testY, validX, validY)
    save_model_result_to_report(dataset_h5_path, feat, 'CNN', acc, pre, rec, f1)

    print('# LSTM')
    clf, acc, pre, rec, f1 = buildLSTM(trainX, trainY, testX, testY, validX, validY)
    save_model_result_to_report(dataset_h5_path, feat, 'LSTM', acc, pre, rec, f1)

    print ('# GRU')
    clf, acc, pre, rec, f1 = buildGRU(trainX, trainY, testX, testY, validX, validY)
    save_model_result_to_report(dataset_h5_path, feat, 'GRU', acc, pre, rec, f1)

    print('# LRCN')
    clf, acc, pre, rec, f1 = buildLRCN(trainX, trainY, testX, testY, validX, validY)
    save_model_result_to_report(dataset_h5_path, feat, 'LRCN', acc, pre, rec, f1)
    return 0


def use_diff_separted_vocal_same_feat_model_compare_it(feat, dataset_h5_path):
    print ('using...', dataset_h5_path)
    log_comment_toFile(dataset_h5_path)
    trainX, trainY, testX, testY, validX, validY = load_dataset(dataset_h5_path)
    trainX, testX = get_feat_need_col(feat, trainX, testX)
    testX, validX = get_feat_need_col(feat, testX, validX)
    # LRCN
    clf, acc, pre, rec, f1 = buildLRCN(trainX, trainY, testX, testY, validX, validY)
    save_model_result_to_report(dataset_h5_path, feat, 'LRCN', acc, pre, rec, f1)
    return 0


def use_diff_post_process_same_feat_model_svs_compare_it(feat, dataset_h5_path):
    for post_method in ['median', 'hmm']:
        print ('using...', dataset_h5_path)
        log_comment_toFile(dataset_h5_path)
        trainX, trainY, testX, testY, validX, validY = load_dataset(dataset_h5_path)
        trainX, testX = get_feat_need_col(feat, trainX, testX)
        testX, validX = get_feat_need_col(feat, testX, validX)
        # LRCN
        clf, acc, pre, rec, f1 = buildLRCN(trainX, trainY, testX, testY, validX, validY, post_method)
        save_model_result_to_report(dataset_h5_path, feat, 'LRCN', acc, pre, rec, f1)
    return 0


def use_diff_dataset_same_feat_model_svs_post_compare_it(feat, dataset_h5_path):
    print ('using...', dataset_h5_path)
    log_comment_toFile(dataset_h5_path)
    trainX, trainY, testX, testY, validX, validY = load_dataset(dataset_h5_path)
    trainX, testX = get_feat_need_col(feat, trainX, testX)
    testX, validX = get_feat_need_col(feat, testX, validX)
    # LRCN
    clf, acc, pre, rec, f1 = buildLRCN(trainX, trainY, testX, testY, validX, validY, 'median')
    save_model_result_to_report(dataset_h5_path, feat, 'LRCN', acc, pre, rec, f1)
    return 0


def batch_run_on_mix_raw_dataset(datasets_dir='../SVS_methods_and_dataset/h5datasets'):
    for dataset in os.listdir(datasets_dir):
        if 'jamendo-raw-mix' in dataset and '.h5' in dataset:
            dataset_h5_path = os.path.join(datasets_dir, dataset)
            print ('using...', dataset_h5_path)
            log_comment_toFile(dataset_h5_path)
            use_diff_features_build_SVM_Model_compare_it(dataset_h5_path)
    return 0


def batch_run_on_mix_raw_dataset_same_feat(datasets_dir='../SVS_methods_and_dataset/h5datasets'):
    for dataset in os.listdir(datasets_dir):
        if 'jamendo-raw-mix.h5' in dataset:
            dataset_h5_path = os.path.join(datasets_dir, dataset)
            print ('using...', dataset_h5_path)
            log_comment_toFile(dataset_h5_path)
            use_diff_models_same_feat_compare_it('mfcc', dataset_h5_path)
    return 0


def batch_run_on_diff_SVS_same_feat_model(datasets_dir='../SVS_methods_and_dataset/h5datasets'):
    need_list = [
        '-FASST', '-RPCA', '-UNET', '-REPET',
        '-zblNMF',
        '-KAML', '-raw-mix']
    for dataset in os.listdir(datasets_dir):
        if 'jamendo' in dataset and '.h5' in dataset:
            if '-' + dataset.split('-')[1] in need_list:
                dataset_h5_path = os.path.join(datasets_dir, dataset)
                print ('using...', dataset_h5_path)
                log_comment_toFile(dataset_h5_path)
                use_diff_separted_vocal_same_feat_model_compare_it('mfcc', dataset_h5_path)
    return 0


def batch_run_on_diff_post_same_feat_model_svs(datasets_dir='../SVS_methods_and_dataset/h5datasets'):
    need_svs = '-UNET'
    for dataset in os.listdir(datasets_dir):
        if 'jamendo' in dataset and '.h5' in dataset:
            if '-' + dataset.split('-')[1] == need_svs:
                dataset_h5_path = os.path.join(datasets_dir, dataset)
                print ('using...', dataset_h5_path)
                log_comment_toFile(dataset_h5_path)
                use_diff_post_process_same_feat_model_svs_compare_it('mfcc', dataset_h5_path)
    return 0


def batch_run_on_diff_dataset_same_feat_model_svs_post(datasets_dir='../SVS_methods_and_dataset/h5datasets'):
    need_svs = 'UNET'
    need_list = ['jamendo', 'MIR-1K', 'RWC', 'iKala']
    for dataset in os.listdir(datasets_dir):
        if need_svs in dataset.split('-') and '.h5' in dataset:
            if dataset.split('_')[0] in need_list or dataset.split('-')[0] in need_list:
                dataset_h5_path = os.path.join(datasets_dir, dataset)
                print ('using...', dataset_h5_path)
                log_comment_toFile(dataset_h5_path)
                use_diff_dataset_same_feat_model_svs_post_compare_it('mfcc', dataset_h5_path)
    return 0


def debug_fine_tune_the_model(datasets_dir='../SVS_methods_and_dataset/h5datasets', feat='mfcc'):
    for dataset in os.listdir(datasets_dir):
        if 'jamendo' in dataset and '.h5' in dataset:
            dataset_h5_path = os.path.join(datasets_dir, dataset)
            print ('using...', dataset_h5_path)
            log_comment_toFile(dataset_h5_path)
            trainX, trainY, testX, testY, validX, validY = load_dataset(dataset_h5_path)
            trainX, trainY, testX, testY = filter_inf_nan(trainX, trainY, testX, testY)
            validX, validY, validX, validY = filter_inf_nan(validX, validY, validX, validY)
            trainX, testX = get_feat_need_col(feat, trainX, testX)
            validX, validX = get_feat_need_col(feat, validX, validX)
            print(trainX[1])
            # scaler = StandardScaler()
            # scaler.fit(trainX)
            # trainX=scaler.transform(trainX)
            # testX = scaler.transform(testX)
            # validX = scaler.transform(validX)
            # print(trainX[1])
            buildDNN(trainX, trainY, testX, testY, validX, validY)
            #####################precision    recall  f1-score   support
            # SVM avg / total       0.88      0.88      0.88     96096
            # DNN avg / total       0.85      0.81      0.81     96096
            # CNN avg / total       0.91      0.91      0.91      2741
            # LSTM avg / total       0.90      0.90      0.90      2741
            # GRU avg / total       0.90      0.90      0.90      2741
            # LRCN avg / total       0.89      0.88      0.88      2741
    return 0


if __name__ == '__main__':
    start = time()
    # batch_run_on_mix_raw_dataset()
    # batch_run_on_mix_raw_dataset_same_feat()
    # batch_run_on_diff_SVS_same_feat_model()
    # batch_run_on_diff_post_same_feat_model_svs()
    # batch_run_on_diff_dataset_same_feat_model_svs_post()
    debug_fine_tune_the_model()
    end = time()
    print('======it takes %.2f s======' % (end - start))
    log_comment_toFile('======it takes %.2f s======' % (end - start))
    pass
