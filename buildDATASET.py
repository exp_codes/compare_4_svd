import os
from time import time, strftime, localtime

import h5py
import numpy
from numpy.random import shuffle
from scipy.io.wavfile import read as wavread
from scipy.io.wavfile import write as wavwrite
from extractFeatures import get_audio_feature


def writeDataset2H5file(trainX, trainY, testX, testY, validX, validY, h5name='dataset'):
    # h5name='../SVS_methods_and_dataset/h5dataset/'+h5name+'.h5'
    to_dir = os.path.dirname(h5name)
    if not os.path.isdir(to_dir) and to_dir:
        os.makedirs(to_dir)
    file = h5py.File(h5name, 'w')
    file.create_dataset('trainX', data=trainX)
    file.create_dataset('trainY', data=trainY)
    file.create_dataset('testX', data=testX)
    file.create_dataset('testY', data=testY)
    if validX != 'none':
        file.create_dataset('validX', data=validX)
        file.create_dataset('validY', data=validY)
    file.close()
    return 0


def log_comment_toFile(comment, logFile='log.txt'):
    logTime = strftime('%Y-%m-%d %H:%M:%S', localtime(time()))
    with open(logFile, 'a')as lf:
        lf.write(logTime + '\t' + comment + '\n')
    return 0


def load_Dataset_2_from_h5file(h5file_path):
    # rwc mir1k ikala
    file = h5py.File(h5file_path, 'r')
    trainX = file['trainX'][:]
    trainY = file['trainY'][:]
    testX = file['testX'][:]
    testY = file['testY'][:]
    file.close()
    return trainX, trainY, testX, testY


def load_Dataset_3_from_h5file(h5file_path):
    # jamendo
    print ('loading h5file...', h5file_path)
    file = h5py.File(h5file_path, 'r')
    trainX = file['trainX'][:]
    trainY = file['trainY'][:]
    testX = file['testX'][:]
    testY = file['testY'][:]
    validX = file['validX'][:]
    validY = file['validY'][:]
    file.close()
    return trainX, trainY, testX, testY, validX, validY


def wav_segment_feature(wavPath, lab_path):
    combineFeat, label_class = None, None

    return combineFeat, label_class


def processJAMENDO(dataset_wav_dir, need_voice='-FASST-voice.wav'):
    trainX, trainY = [], []
    testX, testY = [], []
    validX, validY = [], []

    for root, dirs, names in os.walk(dataset_wav_dir):
        for name in names:
            if '.wav' in name and need_voice in name:
                wavPath = os.path.join(root, name)
                lab_path = dataset_wav_dir + '/tags/' + os.path.basename(wavPath.split(need_voice)[0] + '.lab')
                print (wavPath)
                # print (lab_path)
                sampleRate, audioData = wavread(wavPath)
                with open(lab_path, 'r') as labF:
                    labCont = labF.readlines()
                    for lab_item in labCont:
                        lab_cont_item = lab_item.strip('\n')
                        if lab_cont_item:
                            list_lab_item = lab_cont_item.split(' ')
                            start = int(float(list_lab_item[0]) * sampleRate)
                            end = int(float(list_lab_item[1]) * sampleRate)
                            label_class = 0 if list_lab_item[2] == 'nosing' else 1
                            # print (list_lab_item[2], label_class)
                            segmentData = audioData[start:end]
                            segmentTempPath = wavPath.replace('.wav', '-temp-segment.wav')
                            wavwrite(segmentTempPath, sampleRate, segmentData)
                            try:
                                combineFeat = get_audio_feature(segmentTempPath)
                                os.remove(segmentTempPath)
                                # print (label_class)
                                if '/train/' in wavPath:
                                    trainX.append(combineFeat)
                                    trainY.extend([label_class] * combineFeat.shape[0])
                                elif '/test/' in wavPath:
                                    testX.append(combineFeat)
                                    testY.extend([label_class] * combineFeat.shape[0])
                                elif '/valid/' in wavPath:
                                    validX.append(combineFeat)
                                    validY.extend([label_class] * combineFeat.shape[0])
                            except:
                                with open('error.txt', 'a') as err:
                                    err.write('\nsegment %s %i %i extract feat failed\n' % (wavPath, start, end))
                    clear_temp_segments(dir_data)

    trainX = numpy.concatenate(trainX)
    testX = numpy.concatenate(testX)
    validX = numpy.concatenate(validX)
    h5file_name = dir_data + '/h5datasets/jamendo' + need_voice[:-4] + '.h5'
    writeDataset2H5file(trainX, trainY, testX, testY, validX, validY, h5name=h5file_name)
    return 0


def generate_5_fold_for_cross_validation(all_wav_in_one_dir,
                                         need_voice='-FASST-voice.wav'):
    five_folds = {}
    need_file_list = []
    all_files = os.listdir(all_wav_in_one_dir)
    for item in all_files:
        if need_voice in item:
            need_file_list.append(item)
    total_num = len(need_file_list)
    shuffle(need_file_list)
    first = need_file_list[:int(total_num * 0.2)]
    second = need_file_list[int(total_num * 0.2):int(total_num * 0.4)]
    third = need_file_list[int(total_num * 0.4):int(total_num * 0.6)]
    fourth = need_file_list[int(total_num * 0.6):int(total_num * 0.8)]
    fifth = need_file_list[int(total_num * 0.8):]

    five_folds['first'] = {'train': second + third + fourth + fifth,
                           'test': first}
    five_folds['second'] = {'train': first + third + fourth + fifth,
                            'test': second}
    five_folds['third'] = {'train': second + first + fourth + fifth,
                           'test': third}
    five_folds['fourth'] = {'train': second + third + first + fifth,
                            'test': fourth}
    five_folds['fifth'] = {'train': second + third + fourth + first,
                           'test': fifth}

    return five_folds


def processMIR1K_IKALA_RWC(dataset_wav_dir, need_voice='-FASST-voice.wav'):
    five_folds = generate_5_fold_for_cross_validation(dataset_wav_dir, need_voice)
    for key, value in five_folds.items():
        dataset_name = dataset_wav_dir.split('/')[-2] + '_' + need_voice[:-4] + '_' + str(key)
        trainX, trainY = [], []
        testX, testY = [], []
        train_list = value['train']
        test_list = value['test']
        for train_item in train_list:
            wav_path = os.path.join(dataset_wav_dir, train_item)
            lab_path = wav_path.replace('Wavfile/', 'tags/').split(need_voice)[0] + '.lab'
            print (wav_path)
            # print (lab_path)
            sampleRate, audioData = wavread(wav_path)
            with open(lab_path, 'r') as labF:
                labCont = labF.readlines()
                for lab_item in labCont:
                    lab_cont_item = lab_item.strip('\n')
                    if lab_cont_item:
                        list_lab_item = lab_cont_item.split(' ')
                        start = int(float(list_lab_item[0]) * sampleRate)
                        end = int(float(list_lab_item[1]) * sampleRate)
                        label_class = 0 if list_lab_item[2] == 'nosing' else 1
                        # print (list_lab_item[2], label_class)
                        segmentData = audioData[start:end]
                        segmentTempPath = wav_path.replace('.wav', '-temp-segment.wav')
                        wavwrite(segmentTempPath, sampleRate, segmentData)
                        try:
                            combineFeat = get_audio_feature(segmentTempPath)
                            os.remove(segmentTempPath)
                            trainX.append(combineFeat)
                            trainY.extend([label_class] * combineFeat.shape[0])
                        except:
                            with open('error.txt', 'a') as err:
                                err.write('\nsegment %s %i %i extract feat failed\n' % (wav_path, start, end))
            clear_temp_segments(dir_data)
        for test_item in test_list:
            wav_path = os.path.join(dataset_wav_dir, test_item)
            lab_path = wav_path.replace('Wavfile/', 'tags/').split(need_voice)[0] + '.lab'
            print (wav_path)
            # print (lab_path)
            sampleRate, audioData = wavread(wav_path)
            with open(lab_path, 'r') as labF:
                labCont = labF.readlines()
                for lab_item in labCont:
                    lab_cont_item = lab_item.strip('\n')
                    if lab_cont_item:
                        list_lab_item = lab_cont_item.split(' ')
                        start = int(float(list_lab_item[0]) * sampleRate)
                        end = int(float(list_lab_item[1]) * sampleRate)
                        label_class = 0 if list_lab_item[2] == 'nosing' else 1
                        # print (list_lab_item[2], label_class)
                        segmentData = audioData[start:end]
                        segmentTempPath = wav_path.replace('.wav', '-temp-segment.wav')
                        wavwrite(segmentTempPath, sampleRate, segmentData)
                        try:
                            combineFeat = get_audio_feature(segmentTempPath)
                            os.remove(segmentTempPath)
                            testX.append(combineFeat)
                            testY.extend([label_class] * combineFeat.shape[0])
                        except:
                            with open('error.txt', 'a') as err:
                                err.write('\nsegment %s %i %i extract feat failed\n' % (wav_path, start, end))
                clear_temp_segments(dir_data)
        try:
            trainX = numpy.concatenate(trainX)
            testX = numpy.concatenate(testX)
            h5file_name = dir_data + '/h5datasets/' + dataset_name + '.h5'
            writeDataset2H5file(trainX, trainY, testX, testY, 'none', 'none', h5name=h5file_name)
        except:
            with open('error.txt', 'a') as err:
                err.write('\ngenerate dataset %s wrong with err concatenate it\n' % dataset_name)

    return 0


def batch_process_need_voice():
    need_list = [
        '-FASST-voice.wav', '-RPCA-voice.wav', '-UNET-voice.wav', '-REPET-voice.wav',
        '-zblNMF-voice.wav',
        '-KAML-voice.wav', '-raw-mix.wav']
    # need_list = [
    #     '-raw-mix.wav' ]
    for need_voice in need_list:

        dataset_wav_dir_list = [dir_data + '/MIR-1K/Wavfile',
                                dir_data + '/RWC/Wavfile', dir_data + '/iKala/Wavfile']
        for dataset_wav_dir_item in dataset_wav_dir_list:
            processMIR1K_IKALA_RWC(dataset_wav_dir_item, need_voice)
        processJAMENDO(dataset_wav_dir=dir_data + '/jamendo', need_voice=need_voice)
    return 0


def debug_batch_process_need_voice():
    need_list = ['-raw-mix.wav']
    for need_voice in need_list:
        dataset_wav_dir_list = [dir_data + '/iKala/Wavfile']
        for dataset_wav_dir_item in dataset_wav_dir_list:
            processMIR1K_IKALA_RWC(dataset_wav_dir_item, need_voice)
        processJAMENDO(dataset_wav_dir=dir_data + '/jamendo', need_voice=need_voice)
    return 0


def clear_temp_segments(dir_data='../SVS_methods_and_dataset'):
    for root, dirs, names in os.walk(dir_data):
        for name in names:
            wavPath = os.path.join(root, name)
            if '_temp_lpcc.wav' in wavPath or '-temp-segment.wav' in wavPath:
                os.remove(wavPath)
    return 0


if __name__ == '__main__':
    start = time()
    dir_data = '../SVS_methods_and_dataset'  # '../testDATA'
    batch_process_need_voice()  # extract feature and build all the datasets

    end = time()
    print('it takes %.2f s' % (end - start))
