import os
from pydub import AudioSegment


def convert_to_stere_44100_2_wav_for_KAML(wav_path):
    audio = AudioSegment.from_file(wav_path, 'wav')
    audio = audio.set_channels(2)
    audio = audio.set_frame_rate(44100)
    audio = audio.set_sample_width(2)
    audio.export(wav_path, 'wav')
    print(wav_path, 'done')
    return wav_path


def log_into_file(data_dir='.'):
    with open('log_src_des.txt', 'a') as lsd:
        for root, dirs, names in os.walk(data_dir):
            for name in names:
                file_path = os.path.join(root, name)
                if '.wav' in file_path and 'comparisonSVS' not in file_path:
                    src = file_path
                    des = os.path.join('comparisonSVS/DATA', os.path.basename(src))
                    print(src, des)
                    lsd.write(src + '\t' + des + '\n')
    return 0


def rename_diff_dir_wav_as_log_to_one_dir():
    with open('log_src_des.txt', 'r') as lsd:
        lsdCont = lsd.readlines()
        for item in lsdCont:
            if item != '\n':
                [src, des] = item.strip('\n').split('\t')
                print(src, des)
                os.rename(src, des)
    return 0


def rename_one_dir_back_to_diff_dir_as_log():
    with open('log_src_des_clear.txt', 'r') as lsd:
        lsdCont = lsd.readlines()
        for item in lsdCont:
            if item != '\n':
                for item_method in ['-FASST-music.wav', '-FASST-voice.wav', '-FASST-other.wav', '-KAML-voice.wav',
                                    '-KAML-music.wav', '-raw-mix.wav', '-REPET-music.wav', '-REPET-voice.wav',
                                    '-RPCA-music.wav', '-RPCA-voice.wav', '-UNET-music.wav', '-UNET-voice.wav',
                                    '-zblNMF-music.wav', '-zblNMF-voice.wav']:
                    [des, src] = item.strip('\n').split('\t')
                    src = src.replace('comparisonSVS/DATA/',
                                      '../SVS_methods_and_dataset/comparisonSVS/DATA-OUT/').replace(
                        '.wav', item_method)
                    des = des.replace('./RWCsvd/train/', '../SVS_methods_and_dataset/RWC/Wavfile/') \
                        .replace('./RWCsvd/test/', '../SVS_methods_and_dataset/RWC/Wavfile/') \
                        .replace('./RWCsvd/valid/', '../SVS_methods_and_dataset/RWC/Wavfile/') \
                        .replace('./jamendo/', '../SVS_methods_and_dataset/jamendo/') \
                        .replace('./MIR-1K/', '../SVS_methods_and_dataset/MIR-1K/') \
                        .replace('./iKala/', '../SVS_methods_and_dataset/iKala/') \
                        .replace('.wav', item_method)
                    print(src, des)
                    try:
                        os.rename(src, des)
                    except OSError:
                        with open('rename_back_err.txt', 'a') as rbe:
                            if not os.path.isfile(des):
                                rbe.write(src + '\t' + des + '\n')

    return 0


def delete_unused_data():
    data_dir = 'comparisonSVS/DATA'
    for item in os.listdir(data_dir):
        file_path = os.path.join(data_dir, item)
        if '_music.wav' in file_path or '_vocal.wav' in file_path:
            os.remove(file_path)
    return 0


def batch_convert_to_stere_44100_2_wav_for_KAML():
    data_dir = '../SVS_methods_and_dataset/comparisonSVS/DATA/'
    for item in os.listdir(data_dir):
        file_path = os.path.join(data_dir, item)
        if '.wav' in file_path:
            convert_to_stere_44100_2_wav_for_KAML(file_path)
    return 0


def clear_log_redundent_items():
    final_cont = []
    log_file = "log_src_des.txt"
    with open(log_file, 'r') as logF:
        logCont = logF.readlines()
        for item in logCont:
            if '_music.wav' not in item and '_vocal.wav' not in item:
                final_cont.append(item)

    with open(log_file.replace('.txt', '_clear.txt'), 'w') as logFc:
        logFc.writelines(final_cont)
    return 0


def rename_raw_mix_data_to_16K_mono_wav(dataset_dir='../SVS_methods_and_dataset/comparisonSVS/DATA/'):
    for file_name in os.listdir(dataset_dir):
        file_path = os.path.join(dataset_dir, file_name)
        if '.wav' in file_name:
            sound = AudioSegment.from_wav(file_path)
            sound = sound.set_frame_rate(16000)
            sound = sound.set_sample_width(2)
            sound = sound.set_channels(1)
            sound.export(file_path.replace('/DATA/', '/DATA-OUT/').replace('.wav', '-raw-mix.wav'))

    return 0


def collect_error_svs_mix_wav_to_DATA():
    with open('log_rename_back_err.txt', 'r') as rbe:
        rbeCont = rbe.readlines()
        for item in rbeCont:
            if item != '\n':
                src_des = item.strip('\n').split('\t')
                src = src_des[0]
                des = src_des[1]
                des = des.replace('./RWCsvd/test/', '../SVS_methods_and_dataset/RWC/Wavfile/').replace(
                    './RWCsvd/train/', '../SVS_methods_and_dataset/RWC/Wavfile/').replace('./RWCsvd/valid/',
                                                                                          '../SVS_methods_and_dataset/RWC/Wavfile/')
                print(src, des)
                try:
                    os.rename(src, des)
                    print (src, des)
                except OSError:
                    with open('log_rename_back_err_twice.txt', 'a') as rbe:
                        if not os.path.isfile(des):
                            rbe.write(src + '\t' + des + '\n')
    return 0


def conver_mp3_wav(filename):
    sound = AudioSegment.from_mp3(filename)
    sound = sound.set_channels(1)
    sound = sound.set_sample_width(2)
    sound = sound.set_frame_rate(16000)
    sound.export(filename, 'wav')
    return filename


def batch_conver_mp3_wav(batch_dir='../SVS_methods_and_dataset/'):
    for root, dirs, names in os.walk(batch_dir):
        for name in names:
            if '-raw-mix.wav' in name:
                wav_path = os.path.join(root, name)
                print (wav_path)
                conver_mp3_wav(wav_path)

    return 0


if __name__ == '__main__':
    # delete_unused_data()
    # if not os.path.isfile('log_src_des.txt') or os.path.getsize('log_src_des.txt') == 0:
    #     log_into_file()  # 1. generate log file and log the src des pairs
    #     clear_log_redundent_items()
    # if os.listdir('comparisonSVS/DATA') == []:
    #     rename_diff_dir_wav_as_log_to_one_dir()  # 2.move all wav to DATA dir for processing SVS
    # batch_convert_to_stere_44100_2_wav_for_KAML()  # before the process of the KAML
    # rename_raw_mix_data_to_16K_mono_wav()
    # rename_one_dir_back_to_diff_dir_as_log()  # 3.after all the process then back to diff dirs#TODO
    # collect_error_svs_mix_wav_to_DATA()
    batch_conver_mp3_wav()

    pass
